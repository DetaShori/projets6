<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//@ORM\Index(name="GROUPE_FORMATION_FK", columns={"TYF_code"}),
/**
 * Groupe
 *
 * @ORM\Table(name="GROUPE", indexes={ @ORM\Index(name="GROUPE_TYPE_COURS0_FK", columns={"TYC_code"})})
 * @ORM\Entity
 */
class Groupe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GRO_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $groId;

    /**
     * @var integer
     *
     * @ORM\Column(name="GRO_effectif", type="integer", nullable=false)
     */
    private $groEffectif;

    /**
     * @var integer
     *
     * @ORM\Column(name="GRO_volumePresentielNonAffecte", type="integer", nullable=true)
     */
    private $groVolumepresentielnonaffecte;

    /**
     * @var integer
     *
     * @ORM\Column(name="GRO_volumePresentiel", type="integer", nullable=true)
     */
    private $groVolumepresentiel;

    /*/**
     * @var \Formation
     *
     * @ORM\ManyToOne(targetEntity="Formation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYF_code", referencedColumnName="TYF_code")
     * })
     */
    //private $tyfCode;

    /**
     * @var \TypeCours
     *
     * @ORM\ManyToOne(targetEntity="TypeCours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYC_code", referencedColumnName="TYC_code")
     * })
     */
    private $tycCode;

    /**
     * @return int
     */
    public function getGroId()
    {
        return $this->groId;
    }

    /**
     * @param int $groId
     */
    public function setGroId($groId)
    {
        $this->groId = $groId;
    }

    /**
     * @return int
     */
    public function getGroEffectif()
    {
        return $this->groEffectif;
    }

    /**
     * @param int $groEffectif
     */
    public function setGroEffectif($groEffectif)
    {
        $this->groEffectif = $groEffectif;
    }

    /**
     * @return int
     */
    public function getGroVolumepresentielnonaffecte()
    {
        return $this->groVolumepresentielnonaffecte;
    }

    /**
     * @param int $groVolumepresentielnonaffecte
     */
    public function setGroVolumepresentielnonaffecte($groVolumepresentielnonaffecte)
    {
        $this->groVolumepresentielnonaffecte = $groVolumepresentielnonaffecte;
    }

    /**
     * @return int
     */
    public function getGroVolumepresentiel()
    {
        return $this->groVolumepresentiel;
    }

    /**
     * @param int $groVolumepresentiel
     */
    public function setGroVolumepresentiel($groVolumepresentiel)
    {
        $this->groVolumepresentiel = $groVolumepresentiel;
    }

    /*/**
     * @return \Formation
     */
   /* public function getTyfCode()
    {
        return $this->tyfCode;
    }
*/
    /*/**
     * @param \Formation $tyfCode
     */
   /* public function setTyfCode($tyfCode)
    {
        $this->tyfCode = $tyfCode;
    }
*/
    /**
     * @return \TypeCours
     */
    public function getTycCode()
    {
        return $this->tycCode;
    }

    /**
     * @param \TypeCours $tycCode
     */
    public function setTycCode($tycCode)
    {
        $this->tycCode = $tycCode;
    }


}

