<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatutEnseignant
 *
 * @ORM\Table(name="STATUT_ENSEIGNANT")
 * @ORM\Entity
 */
class StatutEnseignant
{
    /**
     * @var string
     *
     * @ORM\Column(name="STE_code", type="string", length=15, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $steCode;

    /**
     * @var string
     *
     * @ORM\Column(name="STE_libelle", type="string", length=50, nullable=false)
     */
    private $steLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="STE_volAFaireEquiTD", type="integer", nullable=false)
     */
    private $steVolafaireequitd;

    /**
     * @return string
     */
    public function getSteCode()
    {
        return $this->steCode;
    }

    /**
     * @param string $steCode
     */
    public function setSteCode($steCode)
    {
        $this->steCode = $steCode;
    }

    /**
     * @return string
     */
    public function getSteLibelle()
    {
        return $this->steLibelle;
    }

    /**
     * @param string $steLibelle
     */
    public function setSteLibelle($steLibelle)
    {
        $this->steLibelle = $steLibelle;
    }

    /**
     * @return int
     */
    public function getSteVolafaireequitd()
    {
        return $this->steVolafaireequitd;
    }

    /**
     * @param int $steVolafaireequitd
     */
    public function setSteVolafaireequitd($steVolafaireequitd)
    {
        $this->steVolafaireequitd = $steVolafaireequitd;
    }


}

