<?php
/**
 * Created by PhpStorm.
 * User: Deta
 * Date: 13/03/2019
 * Time: 00:41
 */

namespace S6\FirstBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function construct()
    {
        parent::construct();
        // your own logic
    }
}