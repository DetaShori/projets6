<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeFormation
 *
 * @ORM\Table(name="TYPE_FORMATION")
 * @ORM\Entity
 */
class TypeFormation
{
    /**
     * @var string
     *
     * @ORM\Column(name="TYF_code", type="string", length=100, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tyfCode;

    /**
     * @var string
     *
     * @ORM\Column(name="TYF_libelle", type="string", length=100, nullable=false)
     */
    private $tyfLibelle;

    /**
     * @return string
     */
    public function getTyfCode()
    {
        return $this->tyfCode;
    }

    /**
     * @param string $tyfCode
     */
    public function setTyfCode($tyfCode)
    {
        $this->tyfCode = $tyfCode;
    }

    /**
     * @return string
     */
    public function getTyfLibelle()
    {
        return $this->tyfLibelle;
    }

    /**
     * @param string $tyfLibelle
     */
    public function setTyfLibelle($tyfLibelle)
    {
        $this->tyfLibelle = $tyfLibelle;
    }


}

