<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeCours
 *
 * @ORM\Table(name="TYPE_COURS")
 * @ORM\Entity
 */
class TypeCours
{
    /**
     * @var string
     *
     * @ORM\Column(name="TYC_code", type="string", length=4, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tycCode;

    /**
     * @var string
     *
     * @ORM\Column(name="TYC_libelle", type="string", length=50, nullable=false)
     */
    private $tycLibelle;

    /**
     * @return string
     */
    public function getTycCode()
    {
        return $this->tycCode;
    }

    /**
     * @param string $tycCode
     */
    public function setTycCode($tycCode)
    {
        $this->tycCode = $tycCode;
    }

    /**
     * @return string
     */
    public function getTycLibelle()
    {
        return $this->tycLibelle;
    }

    /**
     * @param string $tycLibelle
     */
    public function setTycLibelle($tycLibelle)
    {
        $this->tycLibelle = $tycLibelle;
    }


}

