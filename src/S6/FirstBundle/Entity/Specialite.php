<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Specialite
 *
 * @ORM\Table(name="SPECIALITE")
 * @ORM\Entity
 */
class Specialite
{
    /**
     * @var string
     *
     * @ORM\Column(name="SPE_code", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $speCode;

    /**
     * @var string
     *
     * @ORM\Column(name="SPE_libelle", type="string", length=120, nullable=false)
     */
    private $speLibelle;

    /**
     * @return string
     */
    public function getSpeCode()
    {
        return $this->speCode;
    }

    /**
     * @param string $speCode
     */
    public function setSpeCode($speCode)
    {
        $this->speCode = $speCode;
    }

    /**
     * @return string
     */
    public function getSpeLibelle()
    {
        return $this->speLibelle;
    }

    /**
     * @param string $speLibelle
     */
    public function setSpeLibelle($speLibelle)
    {
        $this->speLibelle = $speLibelle;
    }


}

