<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cours
 *
 * @ORM\Table(name="COURS", indexes={@ORM\Index(name="COURS_UNITE_ENSEIGNEMENT_FK", columns={"UNE_code"}), @ORM\Index(name="COURS_TYPE_COURS0_FK", columns={"TYC_code"}), @ORM\Index(name="COURS_ENSEIGNANT1_FK", columns={"id_ENS"})})
 * @ORM\Entity
 */
class Cours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="COU_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $couId;

    /**
     * @var string
     *
     * @ORM\Column(name="COU_codeELP", type="string", length=50, nullable=false)
     */
    private $couCodeelp;

    /**
     * @var integer
     *
     * @ORM\Column(name="COU_volumeParGroupe", type="integer", nullable=false)
     */
    private $couVolumepargroupe;

    /**
     * @var integer
     *
     * @ORM\Column(name="COU_semestre", type="integer", nullable=false)
     */
    private $couSemestre;

    /**
     * @var \UniteEnseignement
     *
     * @ORM\ManyToOne(targetEntity="UniteEnseignement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UNE_code", referencedColumnName="UNE_code")
     * })
     */
    private $uneCode;

    /**
     * @var \TypeCours
     *
     * @ORM\ManyToOne(targetEntity="TypeCours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYC_code", referencedColumnName="TYC_code")
     * })
     */
    private $tycCode;

    /**
     * @var \Enseignant
     *
     * @ORM\ManyToOne(targetEntity="Enseignant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ENS", referencedColumnName="ENS_id")
     * })
     */
    private $idEns;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Enseignant", inversedBy="cou")
     * @ORM\JoinTable(name="enseigner",
     *   joinColumns={
     *     @ORM\JoinColumn(name="COU_id", referencedColumnName="COU_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ENS_id", referencedColumnName="ENS_id")
     *   }
     * )
     */
    private $ens;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ens = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getCouId()
    {
        return $this->couId;
    }

    /**
     * @param int $couId
     */
    public function setCouId($couId)
    {
        $this->couId = $couId;
    }

    /**
     * @return string
     */
    public function getCouCodeelp()
    {
        return $this->couCodeelp;
    }

    /**
     * @param string $couCodeelp
     */
    public function setCouCodeelp($couCodeelp)
    {
        $this->couCodeelp = $couCodeelp;
    }

    /**
     * @return int
     */
    public function getCouVolumepargroupe()
    {
        return $this->couVolumepargroupe;
    }

    /**
     * @param int $couVolumepargroupe
     */
    public function setCouVolumepargroupe($couVolumepargroupe)
    {
        $this->couVolumepargroupe = $couVolumepargroupe;
    }

    /**
     * @return int
     */
    public function getCouSemestre()
    {
        return $this->couSemestre;
    }

    /**
     * @param int $couSemestre
     */
    public function setCouSemestre($couSemestre)
    {
        $this->couSemestre = $couSemestre;
    }

    /**
     * @return \UniteEnseignement
     */
    public function getUneCode()
    {
        return $this->uneCode;
    }

    /**
     * @param \UniteEnseignement $uneCode
     */
    public function setUneCode($uneCode)
    {
        $this->uneCode = $uneCode;
    }

    /**
     * @return \TypeCours
     */
    public function getTycCode()
    {
        return $this->tycCode;
    }

    /**
     * @param \TypeCours $tycCode
     */
    public function setTycCode($tycCode)
    {
        $this->tycCode = $tycCode;
    }

    /**
     * @return \Enseignant
     */
    public function getIdEns()
    {
        return $this->idEns;
    }

    /**
     * @param \Enseignant $idEns
     */
    public function setIdEns($idEns)
    {
        $this->idEns = $idEns;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEns()
    {
        return $this->ens;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $ens
     */
    public function setEns($ens)
    {
        $this->ens = $ens;
    }

}

