<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Composante
 *
 * @ORM\Table(name="COMPOSANTE")
 * @ORM\Entity
 */
class Composante
{
    /**
     * @var string
     *
     * @ORM\Column(name="COM_code", type="string", length=25, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $comCode;

    /**
     * @var string
     *
     * @ORM\Column(name="COM_libelle", type="string", length=25, nullable=false)
     */
    private $comLibelle;

    /**
     * @return string
     */
    public function getComCode()
    {
        return $this->comCode;
    }

    /**
     * @param string $comCode
     */
    public function setComCode($comCode)
    {
        $this->comCode = $comCode;
    }

    /**
     * @return string
     */
    public function getComLibelle()
    {
        return $this->comLibelle;
    }

    /**
     * @param string $comLibelle
     */
    public function setComLibelle($comLibelle)
    {
        $this->comLibelle = $comLibelle;
    }


}

