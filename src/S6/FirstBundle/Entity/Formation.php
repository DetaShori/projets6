<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//,@ORM\Index(name="FORMATION_TYPE_FORMATION0_FK", columns={"TYF_code"})
/**
 * Formation
 *
 * @ORM\Table(name="FORMATION", indexes={@ORM\Index(name="FORMATION_SPECIALITE_FK", columns={"SPE_code"})})
 * @ORM\Entity
 */
class Formation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FOR_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $forId;

    /**
     * @var string
     *
     * @ORM\Column(name="FOR_codeEtape", type="string", length=50, nullable=false)
     */
    private $forCodeetape;

    /**
     * @var string
     *
     * @ORM\Column(name="FOR_codeUFR", type="string", length=50, nullable=false)
     */
    private $forCodeufr;

    /**
     * @var \Specialite
     *
     * @ORM\ManyToOne(targetEntity="Specialite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SPE_code", referencedColumnName="SPE_code")
     * })
     */
    private $speCode;

    /*/**
     * @var \TypeFormation
     *
     * @ORM\ManyToOne(targetEntity="TypeFormation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYF_code", referencedColumnName="TYF_code")
     * })
     */

   // private $tyfCode;

    /**
     * @return int
     */
    public function getForId()
    {
        return $this->forId;
    }

    /**
     * @param int $forId
     */
    public function setForId($forId)
    {
        $this->forId = $forId;
    }

    /**
     * @return string
     */
    public function getForCodeetape()
    {
        return $this->forCodeetape;
    }

    /**
     * @param string $forCodeetape
     */
    public function setForCodeetape($forCodeetape)
    {
        $this->forCodeetape = $forCodeetape;
    }

    /**
     * @return string
     */
    public function getForCodeufr()
    {
        return $this->forCodeufr;
    }

    /**
     * @param string $forCodeufr
     */
    public function setForCodeufr($forCodeufr)
    {
        $this->forCodeufr = $forCodeufr;
    }

    /**
     * @return \Specialite
     */
    public function getSpeCode()
    {
        return $this->speCode;
    }

    /**
     * @param \Specialite $speCode
     */
    public function setSpeCode($speCode)
    {
        $this->speCode = $speCode;
    }

    /*
    /**
     * @return \TypeFormation
     */
    /*public function getTyfCode()
    {
        return $this->tyfCode;
    }

    /**
     * @param \TypeFormation $tyfCode
     */
   /* public function setTyfCode($tyfCode)
    {
        $this->tyfCode = $tyfCode;
    }
*/

}

