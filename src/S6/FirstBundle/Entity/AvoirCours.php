<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvoirCours
 *
 * @ORM\Table(name="avoir_cours", indexes={@ORM\Index(name="avoir_cours_COURS_FK", columns={"COU_id"}), @ORM\Index(name="avoir_cours_GROUPE1_FK", columns={"GRO_code_GROUPE_mutualisation"}), @ORM\Index(name="IDX_F033F88FBB3B5395", columns={"GRO_id"})})
 * @ORM\Entity
 */
class AvoirCours
{
    /**
     * @var \Cours
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Cours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COU_id", referencedColumnName="COU_id")
     * })
     */
    private $cou;

    /**
     * @var \Groupe
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Groupe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="GRO_id", referencedColumnName="GRO_id")
     * })
     */
    private $gro;

    /**
     * @var \Groupe
     *
     * @ORM\ManyToOne(targetEntity="Groupe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="GRO_code_GROUPE_mutualisation", referencedColumnName="GRO_id")
     * })
     */
    private $groCodeGroupeMutualisation;

    /**
     * @return \Cours
     */
    public function getCou()
    {
        return $this->cou;
    }

    /**
     * @param \Cours $cou
     */
    public function setCou($cou)
    {
        $this->cou = $cou;
    }

    /**
     * @return \Groupe
     */
    public function getGro()
    {
        return $this->gro;
    }

    /**
     * @param \Groupe $gro
     */
    public function setGro($gro)
    {
        $this->gro = $gro;
    }

    /**
     * @return \Groupe
     */
    public function getGroCodeGroupeMutualisation()
    {
        return $this->groCodeGroupeMutualisation;
    }

    /**
     * @param \Groupe $groCodeGroupeMutualisation
     */
    public function setGroCodeGroupeMutualisation($groCodeGroupeMutualisation)
    {
        $this->groCodeGroupeMutualisation = $groCodeGroupeMutualisation;
    }


}

