<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Enseignant
 *
 * @ORM\Table(name="ENSEIGNANT", indexes={@ORM\Index(name="ENSEIGNANT_DROIT_FK", columns={"DRO_niveau"}), @ORM\Index(name="ENSEIGNANT_STATUT_ENSEIGNANT0_FK", columns={"STE_code"}), @ORM\Index(name="ENSEIGNANT_COMPOSANTE1_FK", columns={"COM_code"})})
 * @ORM\Entity
 */
class Enseignant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ENS_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ensId;

    /**
     * @var string
     *
     * @ORM\Column(name="ENS_nom", type="string", length=50, nullable=false)
     */
    private $ensNom;

    /**
     * @var string
     *
     * @ORM\Column(name="ENS_prenom", type="string", length=50, nullable=false)
     */
    private $ensPrenom;

    /**
     * @var float
     *
     * @ORM\Column(name="ENS_volEquiTD", type="float", precision=10, scale=0, nullable=true)
     */
    private $ensVolequitd;

    /**
     * @var float
     *
     * @ORM\Column(name="ENS_volAffecteEquiTD", type="float", precision=10, scale=0, nullable=true)
     */
    private $ensVolaffecteequitd;

    /**
     * @var float
     *
     * @ORM\Column(name="ENS_heuresComplementaires", type="float", precision=10, scale=0, nullable=true)
     */
    private $ensHeurescomplementaires;

    /**
     * @var string
     *
     * @ORM\Column(name="ENS_motDePasse", type="string", length=77, nullable=false)
     */
    private $ensMotdepasse;

    /**
     * @var string
     *
     * @ORM\Column(name="ENS_login", type="string", length=77, nullable=false)
     */
    private $ensLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="ENS_natureDecharge", type="string", length=77, nullable=true)
     */
    private $ensNaturedecharge;

    /**
     * @var float
     *
     * @ORM\Column(name="ENS_volumeDecharge", type="float", precision=10, scale=0, nullable=true)
     */
    private $ensVolumedecharge;

    /**
     * @var \Droit
     *
     * @ORM\ManyToOne(targetEntity="Droit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DRO_niveau", referencedColumnName="DRO_niveau")
     * })
     */
    private $droNiveau;

    /**
     * @var \StatutEnseignant
     *
     * @ORM\ManyToOne(targetEntity="StatutEnseignant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STE_code", referencedColumnName="STE_code")
     * })
     */
    private $steCode;

    /**
     * @var \Composante
     *
     * @ORM\ManyToOne(targetEntity="Composante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COM_code", referencedColumnName="COM_code")
     * })
     */
    private $comCode;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Cours", mappedBy="ens")
     */
    private $cou;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cou = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getEnsId()
    {
        return $this->ensId;
    }

    /**
     * @param int $ensId
     */
    public function setEnsId($ensId)
    {
        $this->ensId = $ensId;
    }

    /**
     * @return string
     */
    public function getEnsNom()
    {
        return $this->ensNom;
    }

    /**
     * @param string $ensNom
     */
    public function setEnsNom($ensNom)
    {
        $this->ensNom = $ensNom;
    }

    /**
     * @return string
     */
    public function getEnsPrenom()
    {
        return $this->ensPrenom;
    }

    /**
     * @param string $ensPrenom
     */
    public function setEnsPrenom($ensPrenom)
    {
        $this->ensPrenom = $ensPrenom;
    }

    /**
     * @return float
     */
    public function getEnsVolequitd()
    {
        return $this->ensVolequitd;
    }

    /**
     * @param float $ensVolequitd
     */
    public function setEnsVolequitd($ensVolequitd)
    {
        $this->ensVolequitd = $ensVolequitd;
    }

    /**
     * @return float
     */
    public function getEnsVolaffecteequitd()
    {
        return $this->ensVolaffecteequitd;
    }

    /**
     * @param float $ensVolaffecteequitd
     */
    public function setEnsVolaffecteequitd($ensVolaffecteequitd)
    {
        $this->ensVolaffecteequitd = $ensVolaffecteequitd;
    }

    /**
     * @return float
     */
    public function getEnsHeurescomplementaires()
    {
        return $this->ensHeurescomplementaires;
    }

    /**
     * @param float $ensHeurescomplementaires
     */
    public function setEnsHeurescomplementaires($ensHeurescomplementaires)
    {
        $this->ensHeurescomplementaires = $ensHeurescomplementaires;
    }

    /**
     * @return string
     */
    public function getEnsMotdepasse()
    {
        return $this->ensMotdepasse;
    }

    /**
     * @param string $ensMotdepasse
     */
    public function setEnsMotdepasse($ensMotdepasse)
    {
        $this->ensMotdepasse = $ensMotdepasse;
    }

    /**
     * @return string
     */
    public function getEnsLogin()
    {
        return $this->ensLogin;
    }

    /**
     * @param string $ensLogin
     */
    public function setEnsLogin($ensLogin)
    {
        $this->ensLogin = $ensLogin;
    }

    /**
     * @return string
     */
    public function getEnsNaturedecharge()
    {
        return $this->ensNaturedecharge;
    }

    /**
     * @param string $ensNaturedecharge
     */
    public function setEnsNaturedecharge($ensNaturedecharge)
    {
        $this->ensNaturedecharge = $ensNaturedecharge;
    }

    /**
     * @return float
     */
    public function getEnsVolumedecharge()
    {
        return $this->ensVolumedecharge;
    }

    /**
     * @param float $ensVolumedecharge
     */
    public function setEnsVolumedecharge($ensVolumedecharge)
    {
        $this->ensVolumedecharge = $ensVolumedecharge;
    }

    /**
     * @return \Droit
     */
    public function getDroNiveau()
    {
        return $this->droNiveau;
    }

    /**
     * @param \Droit $droNiveau
     */
    public function setDroNiveau($droNiveau)
    {
        $this->droNiveau = $droNiveau;
    }

    /**
     * @return \StatutEnseignant
     */
    public function getSteCode()
    {
        return $this->steCode;
    }

    /**
     * @param \StatutEnseignant $steCode
     */
    public function setSteCode($steCode)
    {
        $this->steCode = $steCode;
    }

    /**
     * @return \Composante
     */
    public function getComCode()
    {
        return $this->comCode;
    }

    /**
     * @param \Composante $comCode
     */
    public function setComCode($comCode)
    {
        $this->comCode = $comCode;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCou()
    {
        return $this->cou;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $cou
     */
    public function setCou($cou)
    {
        $this->cou = $cou;
    }

}

