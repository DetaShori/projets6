<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Droit
 *
 * @ORM\Table(name="DROIT")
 * @ORM\Entity
 */
class Droit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="DRO_niveau", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $droNiveau;

    /**
     * @var string
     *
     * @ORM\Column(name="DRO_libelle", type="string", length=25, nullable=false)
     */
    private $droLibelle;

    /**
     * @return int
     */
    public function getDroNiveau()
    {
        return $this->droNiveau;
    }

    /**
     * @param int $droNiveau
     */
    public function setDroNiveau($droNiveau)
    {
        $this->droNiveau = $droNiveau;
    }

    /**
     * @return string
     */
    public function getDroLibelle()
    {
        return $this->droLibelle;
    }

    /**
     * @param string $droLibelle
     */
    public function setDroLibelle($droLibelle)
    {
        $this->droLibelle = $droLibelle;
    }


}

