<?php

namespace S6\FirstBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UniteEnseignement
 *
 * @ORM\Table(name="UNITE_ENSEIGNEMENT")
 * @ORM\Entity
 */
class UniteEnseignement
{
    /**
     * @var string
     *
     * @ORM\Column(name="UNE_code", type="string", length=25, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uneCode;

    /**
     * @var string
     *
     * @ORM\Column(name="UNE_libelle", type="string", length=50, nullable=false)
     */
    private $uneLibelle;

    /**
     * @return string
     */
    public function getUneCode()
    {
        return $this->uneCode;
    }

    /**
     * @param string $uneCode
     */
    public function setUneCode($uneCode)
    {
        $this->uneCode = $uneCode;
    }

    /**
     * @return string
     */
    public function getUneLibelle()
    {
        return $this->uneLibelle;
    }

    /**
     * @param string $uneLibelle
     */
    public function setUneLibelle($uneLibelle)
    {
        $this->uneLibelle = $uneLibelle;
    }


}

