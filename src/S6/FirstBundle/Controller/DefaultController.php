<?php

namespace S6\FirstBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use S6\FirstBundle\Entity\Enseignant;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DefaultController extends Controller
{
    public function indexAction()
    {
       // return $this->render('S6FirstBundle:Default:connexion.html.twig');


        // On crée un objet Advert
        $enseignant = new Enseignant();

        // On crée le FormBuilder grâce au service form factory
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $enseignant);

        // On ajoute les champs de l'entité que l'on veut à notre formulaire
        $formBuilder
            ->add('ensLogin',      TextType::class)
            ->add('ensMotdepasse',     TextType::class)
            ->add('connexion',      SubmitType::class)
        ;
        // Pour l'instant, pas de candidatures, catégories, etc., on les gérera plus tard

        // À partir du formBuilder, on génère le formulaire
        $form = $formBuilder->getForm();

        // On passe la méthode createView() du formulaire à la vue
        // afin qu'elle puisse afficher le formulaire toute seule
        return $this->render('S6FirstBundle:Default:connexion.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
